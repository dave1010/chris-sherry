<?php
$xml = file_get_contents('xml.xml');
$content = simplexml_load_string($xml);
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $content->header->name->firstname; ?> <?php echo $content->header->name->surname; ?> - <?php echo $content->header->role; ?></title>
    <meta name="robots" content="index,nofollow">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">

    <meta name="viewport" content="width=device-width"/>
    <meta name="description" content="LAMP <?php echo $content->header->role; ?>"/>
    <meta charset="UTF-8"/>

    <link type="text/css" rel="stylesheet" href="/css/style.css">
    <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700|Roboto:400,300' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-17526646-1']);
      _gaq.push(['_trackPageview']);

      (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="top">
    <div id="cv">
        <div class="mainDetails">
            <div id="headshot" class="">
                <img src="/images/me.png" alt="Chris Sherry's Headshot" />
            </div>

            <div id="name">
                <h1 class=""><?php echo $content->header->name->firstname; ?> <?php echo $content->header->name->surname; ?></h1>
                <h2 class=""><?php echo $content->header->role; ?></h2>
            </div>

            <div id="contactDetails">
                <ul>
                    <li><img class="social-icon xperia-icon" src="/images/xperia-mini-mini.png"><!--googleoff: all--> <?php echo $content->header->contact->phone; ?><!--googleon: all--> </li>
                    <li><img class="social-icon email-icon" src="/images/email.png"/><!--googleoff: all--><a href='mailt&#111;&#58;%6&#51;&#37;68ris&#64;&#99;&#104;rissh&#101;rry%2Eco&#46;&#117;k'>chris&#64;chri&#115;sherry&#46;co&#46;uk</a><!--googleon: all--></li>
                    <li><img class="social-icon" src="/images/world.png"/> <a href="<?php echo $content->header->contact->url; ?>"><?php echo $content->header->contact->url; ?></a></li>

                    <li><img class="social-icon twitter-icon" src="/images/twitter-bird-callout.png" /><a href="https://twitter.com/<?php echo $content->header->contact->twitter; ?>">@<?php echo $content->header->contact->twitter; ?></a></li>

                </ul>
            </div>
            <div class="clear"></div>
        </div>

        <div id="mainArea" class="">
            <section>


                <ul class="keySkills">
                    <?php foreach ($content->skillarea->skillset->skill as $skill) : ?>
                    <li><?php echo $skill; ?></li>
                <?php endforeach; ?>


            </ul>

            <div class="clear"></div>
        </section>

        <section>
            <?php foreach ($content->history->job as $job) : ?>

            <article class="clearfix">
                <div class="sectionTitle">
                    <h2><?php echo $job->jobtitle; ?>
                        <span class="date"><?php echo $job->period->from->date->month; ?> <?php echo $job->period->from->date->year; ?> -
                            <?php if ($job->period->to->date) : ?>
                            <?php echo $job->period->to->date->month; ?> <?php echo $job->period->to->date->year; ?>  
                        <?php else : ?>
                        Present
                    <?php endif; ?>
                </span>
                <span class="company">
                    <?php echo $job->employer; ?>
                </span>
            </h2>

        </div>
        <div class="sectionContent">
            <?php foreach ($job->description->para as $para) : ?>
            <p>
                <?php echo $para; ?>
            </p>
        <?php endforeach; ?>

    </div>
</article>

<?php endforeach; ?>

<div class="clear"></div>
</section>

<section class="education clearfix">

    <?php foreach ($content->academics->degrees->degree as $degree) : ?>
    <article>
        <div class="sectionTitle">
            <h2>
                <?php echo $degree->level;?>
                <span class="course">
                    <?php $i = 0;?>
                    <?php foreach ($degree->major as $major) : ?>
                    <?php $i++;?>
                    <?php echo $major;?>
                    <?php if ($i < count($degree->major)) : ?>
                    <span class="and">&amp;</span>
                <?php endif;?>

            <?php endforeach;?>
        </span>
        <span class="date"><?php echo $degree->date->month; ?> <?php echo $degree->date->year; ?>
        </span>
        <span class="uni"><?php echo $degree->institution;?></span>
    </h2>
</div>


<div class="sectionContent">
    <?php foreach($degree->annotation->para as $para) : ?>
    <p><?php echo $para;?></p>
<?php endforeach;?>
</div>
</article>
<?php endforeach;?>
</div>
<div class="clear"></div>
</section>
<div class="options no-print">
   <a title="Print" href="javascript:window.print()" class="no-print print" />
   <img src="/images/printer.png"/>
</a>

<a title="Fork on BitBucket" href="https://bitbucket.org/chrissherry/chris-sherry" class="no-print">
    <img src="/images/php_48.png"/>

    <!--<a title="Download as code generated PDF" href="CV.pdf" class="no-print pdf" />
    <img src="/images/file_pdf.png"/>-->
</a>
<a title="Download as XML" href="/xml" class="no-print xml" />
<img src="/images/file_xml.png"/>
</a>

<a title="Download as JSON" href="/json" class="no-print xml" />
<img src="/images/file_json.png"/>
</a>




</a>
</div>
</div>
</div>

</body>
</html>